/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0605;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Main {

  public static void main(String args[]) {
    Operario o = new Operario();
    Gerente g = new Gerente();
    o.setSueldo();
    g.setSueldo();
    System.out.println(o.getSueldo());
    System.out.println(g.getSueldo());
  }
}
/* EJECUCION:
 5
 10
 */
