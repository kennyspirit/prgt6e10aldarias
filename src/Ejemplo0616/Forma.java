/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0616;

/**
 * Fichero: Forma.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
abstract class Forma {

  void identidad() {
    System.out.println(this);
  }

  abstract public String toString();
}
