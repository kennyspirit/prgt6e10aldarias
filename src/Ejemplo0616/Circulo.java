/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0616;

/**
 * Fichero: Circulo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Circulo extends Forma {

  public String toString() {
    return "circulo";
  }

  public static void jerarquia(Object obj) {
    Object o = obj;
    while (o.getClass().getSuperclass() != null) {
      try {
        System.out.println(o.getClass() + " es subclase de "
                + o.getClass().getSuperclass());
        o = o.getClass().getSuperclass().newInstance();
      } catch (InstantiationException e) {
        System.out.println("Imposible instanciar la clase "
                + o.getClass().getSuperclass());
        break;
      } catch (IllegalAccessException e) {
        System.out.println("No hay acceso");
        break;
      }
    }
  }

  public static void main(String args[]) {
    Circulo c = new Circulo();
    Forma f = (Forma) c;
    f.identidad();
    if (f instanceof Circulo) {
      ((Circulo) f).identidad();
    } else if (!(f instanceof Circulo)) {
      System.out.println("f (forma) no es circulo");
    }
    jerarquia(f);
  }
}

/*
 circulo
 circulo
 class Circulo es subclase de class Forma
 Imposible instanciar la clase class Forma
 */
