/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0615;

/**
 * Fichero: Padre.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Padre {

  protected int dato;

  Padre() {
    this(5);
  }

  Padre(int x) {
    dato = x;
  }
}
