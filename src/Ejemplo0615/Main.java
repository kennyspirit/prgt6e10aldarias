/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0615;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Main {

  public static void main(String args[]) {
    Hijo h1 = new Hijo(1);
    h1.getDato();
  }
}
/* EJECUCION:
 Padre: 2
 Hijo: 1
 */
