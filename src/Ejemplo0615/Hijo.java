/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0615;

/**
 * Fichero: Hijo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Hijo extends Padre {

  private int dato;

  Hijo() {
    dato = 3;
  }

  Hijo(int x) {
    super(2);
    dato = x;
  }

  public void getDato() {
    System.out.println("Padre: " + super.dato);
    System.out.println("Hijo: " + this.dato);
  }
}
