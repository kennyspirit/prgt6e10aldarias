/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0610;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Main {

  public static void main(String args[]) {
    Pajaro p1 = new Pajaro();
    p1.setNombre("Jilgero");
    p1.setColor("Rojo");
    System.out.println(p1.getDetalles());

    Loro l1 = new Loro();
    l1.setColor("Verde");
    l1.setPedigri("Amazonas");
    System.out.println(l1.getDetalles());

    Pajaro p2 = new Loro();
    p2.setColor("Verde");
    // p2.setPedigri("Amazonas"); // Error.
    System.out.println(p2.getDetalles()); // getDetalles de loro pq se sobreescribe.

    // Loro l2 = new Pajaro(); // Error

  }
}
/* 
 Nombre: Jilgero
 Color: Rojo

 Nombre: Loro
 Color: Verde
 Pedigri: Amazonas

 Nombre: Loro
 Color: Verde
 Pedigri: null
 */
