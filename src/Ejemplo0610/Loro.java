/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0610;

/**
 * Fichero: Loro.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Loro extends Pajaro {

  protected String pedigri;

  public Loro() {
    setNombre("Loro");
    // pedigri = null;
  }

  public void setPedigri(String p) {
    pedigri = p;
  }

  public String getDetalles() {
    return "Nombre: " + nombre + "\n"
            + "Color: " + color + "\n"
            + "Pedigri: " + pedigri + "\n";
  }
}
