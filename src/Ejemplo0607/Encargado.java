/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0607;

/**
 * Fichero: Encargado.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Encargado extends Empleado {

  public int getSueldo() {
    Double d = new Double(sueldoBase * 1.1);
    return d.intValue();
  }
}
