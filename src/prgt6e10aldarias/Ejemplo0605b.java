/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e10aldarias;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * Fichero: Ejemplo0605b.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-dic-2013
 */
class Ejemplo0605b extends JFrame implements ActionListener {
  // Implementa el interface ActionListener
  // Hereda de la Clase JFrame

  public Ejemplo0605b() {
    setSize(400, 400); // Hereda de JFrame
    JButton boton = new JButton("Start");
    boton.addActionListener(this);
    add(boton);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setVisible(true);
  }

  public void actionPerformed(ActionEvent a) { // Error si se omite la funcion
    for (int i = 0; true; i++) { // Bucle Infinito
      System.out.println(i);
    }
  }

  public static void main(String[] args) {
    new Ejemplo0605b();
  }
}
