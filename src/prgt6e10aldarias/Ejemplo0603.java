/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e10aldarias;

/**
 * Fichero: Ejemplo0603.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Ejemplo0603 {

  public static void main(String args[]) {

    Byte b = new Byte("1");
    Short s = new Short("1");
    Integer i = new Integer("1");
    Long l = new Long("1");
    Float f = new Float("1");

    System.out.println(b.toString() + " " + Byte.TYPE + " " + Byte.MIN_VALUE
            + "  " + Byte.MAX_VALUE);
    System.out.println(s.toString() + " " + s.TYPE + " " + s.MIN_VALUE
            + "  " + s.MAX_VALUE);
    System.out.println(i.toString() + " " + i.TYPE + " " + i.MIN_VALUE
            + "  " + i.MAX_VALUE);
    System.out.println(l.toString() + " " + l.TYPE + " " + l.MIN_VALUE
            + "  " + l.MAX_VALUE);
    System.out.println(f.toString() + " " + f.TYPE + " " + f.MIN_VALUE
            + "  " + f.MAX_VALUE);
  }
}
/* EJECUCION:
 1 byte -128  127
 1 short -32768  32767
 1 int -2147483648  2147483647
 1 long -9223372036854775808  9223372036854775807
 1.0 float 1.4E-45  3.4028235E38
 */
