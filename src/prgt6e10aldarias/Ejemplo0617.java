/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e10aldarias;

/**
 * Fichero: Ejemplo0617.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejemplo0617 {

  public class Interna {

    private int x;

    void setX(int a) {
      x = a;
    }

    int getX() {
      return x;
    }
  }

  public static void main(String args[]) {
    Ejemplo0617 ex = new Ejemplo0617();
    // Intancia de la clase interna
    Ejemplo0617.Interna in = ex.new Interna();
    in.setX(1);
    System.out.println(in.getX());
    // Interna in1 = Interna(); Error
  }
}

/* EJECUCION
 1
 */
