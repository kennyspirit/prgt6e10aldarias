/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e10aldarias;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Fichero: Ejemplo0604.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Ejemplo0604 {

  public static void main(String args[]) {
    GregorianCalendar c = new GregorianCalendar();
    Date d = new Date();
    c.setTime(d);
    System.out.print(c.get(Calendar.DAY_OF_MONTH));
    System.out.print("-");
    System.out.print(c.get(Calendar.MONTH) + 1); // Enero=0
    System.out.print("-");
    System.out.print(c.get(Calendar.YEAR));
    System.out.print(" ");
    System.out.print(c.get(Calendar.HOUR));
    System.out.print(":");
    System.out.println(c.get(Calendar.MINUTE));

  }
}
/* EJECUCION:
 9-12-2011 5:27
 */
