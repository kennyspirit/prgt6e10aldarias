/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e10aldarias;

/**
 * Fichero: Ejemplo0601.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Ejemplo0601 {

    public static void main(String args[]) {
        Integer a = new Integer(4);
        Integer b = new Integer(5);
        Integer c = new Integer(0);
        Integer d = a + b;
        int s;
         
        c = a + b;
        
        s = a.intValue() + b.intValue();
        s = a + b; // Caso especial.
        
        System.out.println(s);
        System.out.println(c.intValue());
        System.out.println(c);
        c = 1; // Cambiamos valor del wrapper .
        System.out.println(d);
    }
}
/* EJECUCION :
 9
 9
 9
 9
 */
    

