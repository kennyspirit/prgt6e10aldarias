package prgt6e10aldarias;

/**
 * Fichero: Ejemplo0605a.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-dic-2013
 */
// Clase Abstracta
abstract class Objeto {

  String titulo;

  public abstract void setTitulo(String t);

  public void showTitulo() {
    System.out.println("Titulo: " + titulo);
  }
}

// Clase que hereda de la clase abstracta objeto.
class Ventana extends Objeto {  // Debe implementar setTitulo

  public void setTitulo(String t) {
    titulo = t;
  }
}

// Inteface
interface Accion {

  void Ejecutar();
}

// Clase que hereda de Ventana e Implementa el interface Accion.
class Ejemplo0605a extends Ventana implements Accion {

  Ejemplo0605a() {
    setTitulo("Titulo");
    Ejecutar();
    showTitulo();
  }

  // Funcion implentada del interface Acccion
  public void Ejecutar() {
    System.out.println("Ejecutar");
  }

  public static void main(String[] args) {
    new Ejemplo0605a();
  }
}
