/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e10aldarias;

/**
 * Fichero: Ejemplo0612.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
// **************************** Superclase
class Ejemplo0612a {

  String nombre;

  Ejemplo0612a() {
    nombre = "Pajaro";
  }

  String getNombre() {
    return nombre;
  }
}

// ***************************Subclase 
class Ejemplo0612b extends Ejemplo0612a {

  String ciudad;

  Ejemplo0612b() {
    nombre = "Pajaro";
    ciudad = "Valencia";
  }
}

// **************************** Main
public class Ejemplo0612 {

  public static void main(String argv[]) {
    Ejemplo0612a pajaro1 = new Ejemplo0612a();
    Ejemplo0612b loro1 = new Ejemplo0612b();
    //loro1 = (loro)pajaro1; // Error
    pajaro1 = loro1;
    System.out.println(pajaro1.getNombre());
    loro1 = (Ejemplo0612b) pajaro1;
    System.out.println(loro1.getNombre());
  }
}
/* EJECUCION:
 Pajaro
 Pajaro
 */
