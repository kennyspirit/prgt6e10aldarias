/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0613;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Main {

  public static void m(Empleado e) {
    System.out.println(e.getNombre());
  }

  public static void main(String args[]) {

    //Persona-Empleado
    Persona em1 = new Empleado();
    em1.setNombre("Empleado1");
    Encargado en1 = new Encargado();
    en1.setNombre("Encargado1");
    m(en1);
    // m(em1); // Error. void m(Empleado e)
    m((Empleado) em1); // Corregido con casting

    // Empleado-Encargado
    Empleado em2 = new Empleado();
    Encargado en2 = new Encargado();
    em2 = en2; // No necesita casting
    en2 = (Encargado) em2; // Necesita casting explicito.
  }
}
/* EJECUCION:
 Encargado1
 Empleado1
 */
