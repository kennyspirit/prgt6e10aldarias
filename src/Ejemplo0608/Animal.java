/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0608;

/**
 * Fichero: Animal.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public abstract class Animal implements Parlanchin {

  public abstract void habla();
}

class Perro extends Animal {

  public void habla() {
    System.out.println("Guau");
  }
}

class Gato extends Animal {

  public void habla() {
    System.out.println("Miau");
  }
}
