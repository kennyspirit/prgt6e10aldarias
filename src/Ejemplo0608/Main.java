/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0608;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Main {

  public static void main(String[] args) {
    Gato gato = new Gato();
    hazleHablar(gato);
    Cucu cucu = new Cucu();
    hazleHablar(cucu);
    try {
      //espera la pulsacion de una tecla y luego RETORNO
      System.in.read();
    } catch (Exception e) {
    }
  }

  static void hazleHablar(Parlanchin sujeto) {
    sujeto.habla();
  }
}
/* EJECUCION:
 Miau
 Cucu, cucu, ..
 */
