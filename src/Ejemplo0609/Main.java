/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0609;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Main {

  public static void main(String args[]) {
    String linea;
    int entero;
    System.out.println("Dame un numero:");
    linea = System.console().readLine();
    entero = Integer.parseInt(linea);
    A a;
    if (entero > 100) {
      a = new A();
    } else {
      a = new B();
    }
    a.metodo();
  }
}
