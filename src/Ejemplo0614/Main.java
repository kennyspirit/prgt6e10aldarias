/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0614;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Main {

  public static void main(String args[]) {
    Hijo h = new Hijo();
    h.mostrar();
    h.getDato();

  }
}
/* EJECUCION:
 Hijo
 Hijo
 Padre
 10
 */
