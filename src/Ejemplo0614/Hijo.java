/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo0614;

/**
 * Fichero: Hijo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Hijo extends Padre {

  private int dato;

  public void m() {
    System.out.println("Hijo");
    super.dato = 10;
    dato = 20;
  }

  public void getDato() {
    // dato del padre
    System.out.println(super.dato);
  }

  public void mostrar() {
    this.m();  // Hijo
    m();       // Hijo
    super.m(); // Padre
  }
}
